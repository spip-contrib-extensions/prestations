<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'explication_objets_actifs' => 'Sur quels objets éditoriaux proposer les prestations ?',

	// L
	'label_objets_actifs' => 'Objets',

	// P
	'prestations_titre' => 'Prestations',

	// T
	'titre_page_configurer_prestations' => 'Configuration des prestations',
);
