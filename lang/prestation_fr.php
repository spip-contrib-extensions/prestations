<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_prestation' => 'Ajouter cette prestation',

	// C
	'champ_id_prestation_label' => 'Identifiant du parent',
	'champ_id_prestations_type_label' => 'Type de prestation',
	'champ_id_prestations_unite_label' => 'Unité',
	'champ_prestation_label' => 'Objet du parent',
	'champ_prix_unitaire_ht_label' => 'Prix unitaire HT',
	'champ_quantite_label' => 'Quantité',
	'champ_quantite_relative_label' => 'Quantité relative',
	'champ_quantite_relative_rang_label' => 'Relative uniquement aux prestations qui précèdent',
	'champ_quantite_relative_type_label' => 'Relative uniquement aux prestations du même type',
	'champ_rang_label' => 'Rang',
	'champ_taxe_label' => 'TVA',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_prestation' => 'Confirmez-vous la suppression de cette prestation ?',

	// I
	'icone_creer_prestation' => 'Créer une prestation',
	'icone_modifier_prestation' => 'Modifier cette prestation',
	'info_1_prestation' => 'Une prestation',
	'info_aucun_prestation' => 'Aucune prestation',
	'info_nb_prestations' => '@nb@ prestations',
	'info_prestations_auteur' => 'Les prestations de cet auteur',

	// R
	'retirer_lien_prestation' => 'Retirer cette prestation',
	'retirer_tous_liens_prestations' => 'Retirer toutes les prestations',

	// S
	'supprimer_prestation' => 'Supprimer cette prestation',

	// T
	'texte_ajouter_prestation' => 'Ajouter une prestation',
	'texte_changer_statut_prestation' => 'Cette prestation est :',
	'texte_creer_associer_prestation' => 'Créer et associer une prestation',
	'texte_definir_comme_traduction_prestation' => 'Cette prestation est une traduction de la prestation numéro :',
	'titre_langue_prestation' => 'Langue de cette prestation',
	'titre_logo_prestation' => 'Logo de cette prestation',
	'titre_objets_lies_prestation' => 'Liés à cette prestation',
	'titre_prestation' => 'Prestation',
	'titre_prestations' => 'Prestations',
	'titre_prestations_rubrique' => 'Prestations de la rubrique',
);
