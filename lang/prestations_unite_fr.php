<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_prestations_unite' => 'Ajouter cette unité',

	// C
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_prestations_unite' => 'Confirmez-vous la suppression de cette unité ?',

	// I
	'icone_creer_prestations_unite' => 'Créer une unité',
	'icone_modifier_prestations_unite' => 'Modifier cette unité',
	'info_1_prestations_unite' => 'Une unité',
	'info_aucun_prestations_unite' => 'Aucune unité',
	'info_nb_prestations_unites' => '@nb@ unités',
	'info_prestations_unites_auteur' => 'Les unités de cet auteur',

	// R
	'retirer_lien_prestations_unite' => 'Retirer cette unité',
	'retirer_tous_liens_prestations_unites' => 'Retirer toutes les unités',

	// S
	'supprimer_prestations_unite' => 'Supprimer cette unité',

	// T
	'texte_ajouter_prestations_unite' => 'Ajouter une unité',
	'texte_changer_statut_prestations_unite' => 'Cette unité est :',
	'texte_creer_associer_prestations_unite' => 'Créer et associer une unité',
	'texte_definir_comme_traduction_prestations_unite' => 'Cette unité est une traduction de la unité numéro :',
	'titre_langue_prestations_unite' => 'Langue de cette unité',
	'titre_logo_prestations_unite' => 'Logo de cette unité',
	'titre_objets_lies_prestations_unite' => 'Liés à cette unité',
	'titre_prestations_unite' => 'Unité',
	'titre_prestations_unites' => 'Unités',
	'titre_prestations_unites_rubrique' => 'Unités de la rubrique',
	
	// U
	'unite_forfait' => 'Forfait',
	'unite_heures' => 'Heure(s)',
	'unite_jours' => 'Jour(s)',
);
