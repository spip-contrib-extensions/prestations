<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ajouter_lien_prestations_type' => 'Ajouter ce type de prestation',

	// C
	'champ_id_prestations_unite_label' => 'Unité',
	'champ_prix_unitaire_ht_label' => 'Prix unitaire HT',
	'champ_taxe_label' => 'Taxe',
	'champ_titre_label' => 'Titre',
	'confirmer_supprimer_prestations_type' => 'Confirmez-vous la suppression de ce type de prestation ?',

	// I
	'icone_creer_prestations_type' => 'Créer un type de prestation',
	'icone_modifier_prestations_type' => 'Modifier ce type de prestation',
	'info_1_prestations_type' => 'Un type de prestation',
	'info_aucun_prestations_type' => 'Aucun type de prestation',
	'info_nb_prestations_types' => '@nb@ types de prestation',
	'info_prestations_types_auteur' => 'Les types de prestation de cet auteur',

	// R
	'retirer_lien_prestations_type' => 'Retirer ce type de prestation',
	'retirer_tous_liens_prestations_types' => 'Retirer tous les types de prestation',

	// S
	'supprimer_prestations_type' => 'Supprimer ce type de prestation',

	// T
	'texte_ajouter_prestations_type' => 'Ajouter un type de prestation',
	'texte_changer_statut_prestations_type' => 'Ce type de prestation est :',
	'texte_creer_associer_prestations_type' => 'Créer et associer un type de prestation',
	'texte_definir_comme_traduction_prestations_type' => 'Ce type de prestation est une traduction du type de prestation numéro :',
	'titre_langue_prestations_type' => 'Langue de ce type de prestation',
	'titre_logo_prestations_type' => 'Logo de ce type de prestation',
	'titre_objets_lies_prestations_type' => 'Liés à ce type de prestation',
	'titre_prestations_type' => 'Type de prestation',
	'titre_prestations_types' => 'Types de prestation',
	'titre_prestations_types_rubrique' => 'Types de prestation de la rubrique',
);
