<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'prestations_description' => 'Lister des choses à faire dans un projet et possiblement leur prix.',
	'prestations_nom' => 'Prestations',
	'prestations_slogan' => '',
);
